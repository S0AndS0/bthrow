## Example

```
netikras:/tmp$ source error.bash
netikras:/tmp$ 
netikras:/tmp$ calc() { local n1="${1:?$(throw NumberMissingException "Cannot calc - missing number1")}"; local sign="${2:?$(throw OperatorMissingExcetion "Cannot calc - operator missing")}"; local n2="${3:?$(throw NumberMissingException "Cannot calc - missing number2")}"; echo "${n1} ${sign} ${n2}" | bc -l; }
netikras:/tmp$ 
netikras:/tmp$ 
netikras:/tmp$ calc 2 + 7
9
netikras:/tmp$ 
netikras:/tmp$ 
netikras:/tmp$ calc 2 + 
Error[NumberMissingException]: Cannot calc - missing number2
    at error.bash#throw:1
    at main#calc:0
bash: 3: 
netikras:/tmp$ 
netikras:/tmp$ 
netikras:/tmp$ calc
Error[NumberMissingException]: Cannot calc - missing number1
    at error.bash#throw:1
    at main#calc:0
bash: 1: 
netikras:/tmp$ 
netikras:/tmp$ 
netikras:/tmp$ calc 4
Error[OperatorMissingExcetion]: Cannot calc - operator missing
    at error.bash#throw:1
    at main#calc:0
bash: 2: 
netikras:/tmp$ 

```

Exploded function:

```
calc() { 
    local n1="${1:?$(throw NumberMissingException "Cannot calc - missing number1")}"; 
    local sign="${2:?$(throw OperatorMissingExcetion "Cannot calc - operator missing")}"; 
    local n2="${3:?$(throw NumberMissingException "Cannot calc - missing number2")}"; 
    
    echo "${n1} ${sign} ${n2}" | bc -l; 
}
```
