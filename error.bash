#!/bin/bash


error::log() {
    local code="${1:-}";
    local message="${2:-}";
    local stack="${3:-}";

    printf >&2 'Error%s%s\n' "${code:+[${code}]}" "${message:+: ${message}}";

    (( ${#stack} )) && {
        printf >&2 "%s\n" "${stack}";
    }
}


error::get_stack() {
    local offset="${1:-1}";
    local -n _ref_stack="${2:?No stack variable reference provided}"

    local NL=$'\n';
    local STACK;
    local depth=${#FUNCNAME[@]};
    local d=${offset};
    local d_prev=$((d-1));
    local lineno;

    while [[ ${d} -lt "${depth}" ]] ; do
        lineno=${BASH_LINENO[d_prev]};
        STACK="${STACK}    at ${BASH_SOURCE[d]}#${FUNCNAME[d]}:${lineno}${NL}";
        d_prev=${d};
        d=$((d+1));
    done

    # shellcheck disable=SC2034
    _ref_stack="${STACK%${NL}*}";
}


error::throw() {
    local code="${1:-}";
    local message="${2:-}";

    local stack;
    error::get_stack 2 'stack';
    error::log "${code}" "${message}" "${stack}";
}


throw() {
    local code="${?:-0}"

    if (( ${#2} )); then
        code="${1:-}"
        local message="${2:-}"
    else
        local message="${1:-}"
    fi

    error::throw "${code}" "${message}";
}
